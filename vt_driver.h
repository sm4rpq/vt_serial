/*********************************************************************************
 * VT100 and like terminal parser. Allows the use of editing keys on a VT100 terminal or
 * emulator to be used to get proper edit functionality on input.
 * 
 * Supported edit keys:
 * <-      Left arrow moves cursor left (non editing)
 * ->      Right arror moves cursor right (non editing)
 * ^       Up arrow recalls last input string
 * v       Down arrow clears input
 * Insert  Toggles between insert and overwrite mode
 * Delete  Deletes the character under the cursor. Characters to the right are left shifted
 * Backspc Deletes the character to the left of the cursor. Characters to the right are left shifted
 * 
 * A warning about the Esc key. On pre VT100 terminals the esc key actually sent the esc character. On VT100 or later
 * it should not. But at least teraterm does unless remapped. Esc is used to inialize interpretation of other key sequences
 * so if Esc is pressed characters after that will be interpreted as esc sequences and will not end up in the input string.
 * Do not press Esc!
 * 
 * For information on the use of function calls, see below
 *********************************************************************************/

#include <Arduino.h>

#pragma once

//In Ada I would have declared this as a generic with the io-stream and buffer sizes as parameters, but this is C++
//so the alternative is templates, which requires the whole class definition to be in the .h-file. I do not consider
//that to be an option. Next alternative is as done here. Pass to the constructor and do dynamic memory allocation.
class vtDriver
{
  public:
    //Constructor
    //Call this with a pointer to the stream object to use. For instance &Serial1 for the first hardware serial port on a teensy or just &Serial for the
    //USB-serial port.
    vtDriver(Stream *rootStream, int bufSize); //Remember to reserv 1 more than max string length.

    //Functions
    //Main function. Call this to receive the available input characters. Will exit when input stream is empty
    void parse();

    //Sends a escape sequence to the terminal to clear the screen
    void cls();

    //Position the cursor at row y and column x
    void gotoXY(int x, int y);

    //Returns the last entered character (non blocking)
    char getLastChar();

    //Returns a line from the terminal. Blocks until CR is pressed. Full line editing supported using edit keys.
    //Note that in true VMS manner the CR-key will only do Carriage Return. Not line feed. Any output after calling
    //getInputLine must advance to the next line before printing if you do not want to overwrite what the user typed.
    String getInputLine();

    //Determines if input is to be echoed to the user or not. Default is that
    //echoing is turned on.
    void enableEcho(bool bEnable);

  private:
    enum t_parsemode {
          insert,    //State insert character
          ESC,       //State ESC character received
          CSI,       //State Command Set Initializer received.
          DCS,       //State Device Control Sequence received
          SS3};      //State SS3 Sequence received

    Stream* p_rootStream;     //Pointer to the stream object to use as the actual hardware interface
    int m_bufSize;
    int m_bufSpace;
    
    t_parsemode m_parseMode;  //Parser is state machine based. This is the state variable
    int editpoint;            //Insertion point, this is where the next character entered will go.
    int endpoint;             //Index of last character in string. Is -1 if no characters, Is set == bufspace if string is full.
    bool insertmode;          //State flag for insert or overwrite mode
    char csichar;             //Buffer for CSI commands. Some CSI sequences are not to be interpreted immediately but need a terminating ~
    char m_lastChar;          //Buffer for last received character. Used by getLastChar()
    String m_lastLine;        //Buffer for last line. Used by up-arrow to recall previous command
    bool m_enableEcho;        //State flag for echo mode. Used to prevent echoing when echo disabled.
    char* buf;                //Internal buffer for edit string

    //Internal parser functions
    void parseIns(char c);
    void parseEsc(char c);
    void parseCSI(char c);
    void parseDCS(char c);
      
};

