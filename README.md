# vt_serial
Arduino library to parse escape codes from a serial terminal or terminal emulator

All development was done on a teensy 3.2 so no guarantees for working anywhere else.

Should be initialized with the serial port to use and a buffer size. Buffer allocated from heap.
See vt_test.ino



