#include "vt_driver.h"

vtDriver terminal(&Serial1,80);

String str;

void setup() {
  Serial1.begin(9600);
  terminal.cls();
  

}

///////////////////////////////////

void loop()
{
  terminal.enableEcho(true);
  Serial1.println("getInputLine with echo enabled. Type EXIT to continue");
  do{
    str = terminal.getInputLine();
    Serial1.print("You typed:");
    Serial1.println(str);
  } while (!str.equals("EXIT"));

  Serial1.println();
  terminal.enableEcho(false);
  Serial1.println("getInputLine with echo disabled. Type EXIT to continue");
  do{
    str = terminal.getInputLine();
    Serial1.print("You typed:");
    Serial1.println(str);
  } while (!str.equals("EXIT"));
  
  terminal.enableEcho(true);

  Serial1.print("raw character input. Type ^C to exit");
  char chr;
  do
  {
    chr = terminal.getLastChar();
    if (chr!=0){
      if ((chr<33) || (chr>=126)) {
        Serial1.print("#");
        Serial1.println((uint8_t)chr);
      } else{
        Serial1.print(chr);
      }
    }
  } while (chr!=3);
  Serial1.println("Done");
}
